### MAC0110 - MiniEP7
### ALuno: Rodrigo Volpe Battistin
### NUSP: 11795464

## Parte 1) Funções trigonométricas seno, cosseno e tangente

function bernoulli(n) #Função auxiliar
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0 : n
		A[m + 1] = 1 // (m + 1)
		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return abs(A[1])
end

erro = 0.001

function sin(x)
	x = big(x)
	soma = x
	i = 3
	n = 1
	while abs( soma - Base.sin(x) ) > erro && n <= 10
		soma += ( (-1)^n ) * (x^i) / factorial(i)
		n += 1
		i += 2
	end
	return soma
end

function cos(x)
	x = big(x)
	soma = 1
	i = 2
	n = 1
	while abs( soma - Base.cos(x) ) > erro && n <= 10
		soma += ( (-1)^n ) * (x^i) / factorial(i)
		n += 1
		i += 2
	end
	return soma
end

function termotan(x, n) #Função auxiliar de tan(x)
	numerador = big( ( 2^(2*n) ) * ( 2^(2*n) - 1 ) * ( bernoulli(n) ) * x^(2*n -1) )
	denominador = factorial( big(2*n) )
	termo = big(numerador/denominador)
	return termo
end

import Base.tan

function tan(x)
	if abs(x) >= π/2
		println("Essa função só trabalha com valores pertencentes a ]3π/2, π/2[")
	else
		x = big(x)
		soma = x
		n = 2
		i = 1
		while abs( soma - Base.tan(x) ) > erro && i <= 10
			soma = big( soma + termotan(x, n) )
			n += 1
			i += 1
		end
		return soma
	end
end

## Parte 2) Testes das funções

function quaseigual(v1, v2) #Função para testes
	erro = 0.001
	igual = abs(v1 - v2)
	if igual <= erro
		return true
	else
		return false
	end
end

function check_sin(value, x)
	return quaseigual( value, Base.sin(x) )
end

function check_cos(value, x)
	return quaseigual( value, Base.cos(x) )
end

function check_tan(value, x)
	return quaseigual( value, Base.tan(x) )
end

## Parte 3) Agora sim, testando as funções

using Test
function test()
	@test check_sin( sin(0), 0) == true
	@test check_sin( sin(π/6), π/6) == true
	@test check_sin( sin(π/2), π/2) == true
	@test check_sin( sin(π), π) == true
	
	@test check_cos( cos(0), 0) == true
	@test check_cos( cos(π/3), π/3) == true
	@test check_cos( cos(π/2), π/2) == true
	@test check_cos( cos(π), π) == true
	
	@test check_tan( tan(0), 0) == true
	@test check_tan( tan(π/6), π/6) == true
	@test check_tan( tan(π/4), π/4) == true
	@test check_tan( tan(π/3), π/3) == true
	
	println("Acabaram os testes")
end
#test()

## Renomeando funções

function taylor_sin(x)
	x = big(x)
	soma = x
	i = 3
	n = 1
	while abs( soma - Base.sin(x) ) > erro && n <= 10
		soma += ( (-1)^n ) * (x^i) / factorial(i)
		n += 1
		i += 2
	end
	return soma
end

function taylor_cos(x)
	x = big(x)
	soma = 1
	i = 2
	n = 1
	while abs( soma - Base.cos(x) ) > erro && n <= 10
		soma += ( (-1)^n ) * (x^i) / factorial(i)
		n += 1
		i += 2
	end
	return soma
end

function termotaylortan(x, n) #Função auxiliar de taylor_tan(x)
	numerador = big( ( 2^(2*n) ) * ( 2^(2*n) - 1 ) * ( bernoulli(n) ) * x^(2*n -1) )
	denominador = factorial( big(2*n) )
	termo = big(numerador/denominador)
	return termo
end

import Base.tan

function taylor_tan(x)
	if abs(x) >= π/2
		println("Essa função só trabalha com valores pertencentes a ]3π/2, π/2[")
	else
		x = big(x)
		soma = x
		n = 2
		i = 1
		while abs( soma - Base.tan(x) ) > erro && i <= 10
			soma = big( soma + termotaylortan(x, n) )
			n += 1
			i += 1
		end
		return soma
	end
end
