## Testando as funções

using Test
include("trigonometria.jl")

function testfunc()
	println("Testando as funções:")
	@test quaseigual( taylor_sin(0), 0 ) == true
	@test quaseigual( taylor_sin(π/6), 1/2 ) == true
	@test quaseigual( taylor_sin(π/4), sqrt(2)/2 ) == true
	@test quaseigual( taylor_sin(π/3), sqrt(3)/2 ) == true
	@test quaseigual( taylor_sin(π/2), 1 ) == true
	@test quaseigual( taylor_sin(π), 0 ) == true

	@test quaseigual( taylor_cos(0), 1 ) == true
	@test quaseigual( taylor_cos(π/6), sqrt(3)/2 ) == true
	@test quaseigual( taylor_cos(π/4), sqrt(2)/2 ) == true
	@test quaseigual( taylor_cos(π/3), 1/2 ) == true
	@test quaseigual( taylor_cos(π/2), 0 ) == true
	@test quaseigual( taylor_cos(π), -1 ) == true

	@test quaseigual( taylor_tan(0), 0 ) == true
	@test quaseigual( taylor_tan(π/6), sqrt(3)/3 ) == true
	@test quaseigual( taylor_tan(π/4), 1 ) == true
	@test quaseigual( taylor_tan(π/3), sqrt(3) ) == true
	@test quaseigual( taylor_tan(π/5), Base.tan(π/5) ) == true
	@test quaseigual( taylor_tan(π/7), Base.tan(π/7) ) == true
	@test quaseigual( taylor_tan(π/10), Base.tan(π/10) ) == true
	@test taylor_tan(π/2) == nothing
	@test taylor_tan(π) == nothing

	@test quaseigual( taylor_tan(2π/5), Base.tan(2π/5) ) == true
	@test quaseigual( taylor_tan(9π/20), Base.tan(9π/20) ) == true
	@test quaseigual( taylor_tan(49π/50), Base.tan(49π/50) ) == true

	println("Acabaram os testes")
end

function testcheck()
	println("Testando as avaliações:")
	@test check_sin(0.5, π/6) == true
	@test check_cos(0.5, π/3) == true
	@test check_tan(0, π) == true
	@test check_sin(42, π) == false
	@test check_cos(42, π) == false
	@test check_tan(42, π/4) == false

	println("Acabaram os testes")
end

testfunc()
testcheck()
